#!/bin/sh
nohup java -cp "./target/mobile-%PROJECT_VERSION%/mobile-%PROJECT_VERSION%/lib/*:."  com.academy.mobile.Application --spring.config.location=file:./target/mobile-%PROJECT_VERSION%/mobile-%PROJECT_VERSION%/cfg/application.properties $1 $2 $3 &
